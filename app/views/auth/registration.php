<div class="content">

	<header>
	<br>
		<center><h3>Registare as A new Member</h3></center>
	<br>
	</header>
	<form role="form" method="POST" class="interact well well-lg" action="<?php echo BASE_URL?>/Register/registerNewMember">
        <div class="form-group row">
            <?php
            if(!empty($data[0])){
                foreach ($data[0] as $key){
            ?>
                <label class="col-sm-2 text-danger "></label>
                <label class="col-sm-10  text-danger "><?Php echo $key ?></label>
            <?php
                }
            }
            ?>
        </div>
		<div class="form-group row">
		    <label for="name" class="col-sm-2 col-form-label">Name<span class="text-danger"> *</span></label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="name" placeholder="Name" name="name" required>
		    </div>
		</div>
		
		<div class="form-group row">
		    <label for="bio" class="col-sm-2 col-form-label">Bio-Data<span class="text-danger"> *</span></label>
		    <div class="col-sm-10">
		    	<textarea class="form-control"
	                  placeholder="Bio-Data"
	                  id="bio" name="bio" required
	                  data-validation-required-message="Please enter Bio-Data">
                  	
                 </textarea>
		    </div>
		</div>
		
		
		<div class="form-group row">
		    <label for="email" class="col-sm-2 col-form-label">Email<span class="text-danger"> *</span></label>
		    <div class="col-sm-10">
		    	<input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
		    </div>
		</div>
		<div class="form-group row">
		    <label for="inputPassword" class="col-sm-2 col-form-label">Password<span class="text-danger"> *</span></label>
		    <div class="col-sm-10">
		    	<input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" required>
		    </div>
		</div>
		<div class="form-group row">
		    <label for="confirmPassword" class="col-sm-2 col-form-label">Confirm Password<span class="text-danger"> *</span></label>
		    <div class="col-sm-10">
		    	<input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" name="confirmPassword" required>
		    </div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Gender<span class="text-danger"> *</span></label>
			<div class="col-sm-10">
				<select class="form-control" name="sex" id="sex" size="1">
					<option value="0" >select Gender</option>
                    <?php
                    foreach ($data[1] as $key => $value){
                    ?>
					<option value="<?php echo $value['sex']?>"><?php echo $value['sex'] ?></option>
                        <?php
                    }
                    ?>
				</select>
			</div>
		</div>
		
		<div>
			<center>
				<button type="submit" name="register" class="btn btn-outline-customs">Register</button>
			</center>
		</div>
	</form>
</div>
