<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link href="<?php echo CSS_DIR ?>/bootstrap.min.css" rel="stylesheet" type="text/css" />


        <!-- Latest compiled and minified CSS -->
        <link href="<?php echo CSS_DIR ?>/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

        <!--Customs css goes here-->
        <link href="<?php echo CSS_DIR ?>/main.css" rel="stylesheet" type="text/css" />

        <!--Tab icon for the website-->
        <link rel="icon" href="">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!--Here goes the title of the website-->
        <TITLE><?php echo "$pageName"?></TITLE>
        
        
    </head>

<style>
        
    .search-form .form-group {
      float: right !important;
      transition: all 0.35s, border-radius 0s;
      width: 32px;
      height: 32px;
      background-color: #fff;
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
      border-radius: 25px;
      border: 1px solid #ccc;
    }
    .search-form .form-group input.form-control {
      padding-right: 20px;
      border: 0 none;
      background: transparent;
      box-shadow: none;
      display:block;
    }
    .search-form .form-group input.form-control::-webkit-input-placeholder {
      display: none;
    }
    .search-form .form-group input.form-control:-moz-placeholder {
      /* Firefox 18- */
      display: none;
    }
    .search-form .form-group input.form-control::-moz-placeholder {
      /* Firefox 19+ */
      display: none;
    }
    .search-form .form-group input.form-control:-ms-input-placeholder {
      display: none;
    }
    .search-form .form-group:hover,
    .search-form .form-group.hover {
      width: 100%;
      border-radius: 4px 25px 25px 4px;
    }
    .search-form .form-group span.form-control-feedback {
      position: absolute;
      top: -1px;
      right: -2px;
      z-index: 2;
      display: block;
      width: 34px;
      height: 34px;
      line-height: 34px;
      text-align: center;
      color: #3596e0;
      left: initial;
      font-size: 14px;
    }
</style>
    <body>
    <!--your HTML design goes here-->
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top " >
            
            <button class="navbar-toggler mr-auto" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <?php
                if (session_status() == PHP_SESSION_NONE) {
                    Session::init();
                }
                if(Session::get("login") == true) {
                  if(Session::get("is_admin") == 1){ ?>
                        
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>"><b>Class Forum</b></a>
                            </li>
                            
                        </ul>
                        <ul class="navbar-nav navbar-right mr-auto">
                            <li class="nav-item">
                                
                                    
                                <form action="<?php echo BASE_DIR ?>/Search" class="search-form">
                                    <div class="form-group has-feedback">
                                        <label for="search" class="sr-only">Search</label>
                                        <input type="text" class="form-control" name="search" id="search" placeholder="Searching">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </form>
                                       
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/Dashboard/dashboard"><b>Dashboard</b></a>
                            
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/ThreadCreate" title="Create Thread">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    <b>Create</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/Profile"><b><?php echo Session::get('name'); ?></b></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/Login/logOut"><b>Log out</b></a>
                            </li>
                        </ul>
                <?php
                  }
                  else if(Session::get("is_approved") == 1) { ?>

                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item">
                              <a class="nav-link" href="<?php echo BASE_DIR ?>"><b>Class Forum</b></a>
                          </li>
                          
                        </ul>
                        <ul class="navbar-nav navbar-right mr-auto">
                            <li class="nav-item">
                                
                                    
                                <form action="<?php echo BASE_DIR ?>/Search" class="search-form">
                                    <div class="form-group has-feedback">
                                        <label for="search" class="sr-only">Search</label>
                                        <input type="text" class="form-control" name="search" id="search" placeholder="Searching">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </form>
                                       
                                
                            </li>
                            

                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/ThreadCreate" title="Create Thread">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    <b>Create</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/Profile"><b><?php echo Session::get('name'); ?></b></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo BASE_DIR ?>/Login/logOut"><b>Log out</b></a>
                            </li>
                        </ul>





              <?php } } else{ ?>
                    <!--Navbar for Public-->

            
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo BASE_DIR ?>"><b>Class Forum</b> </a>
                            
                        </li>
 
                    </ul>
                    <ul class="navbar-nav navbar-right mr-auto">
                        
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo BASE_DIR ?>/Register/Index"><b>Register</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo BASE_DIR ?>/Login/index"><b>Log in</b></a>
                        </li>
                    </ul>
                <?php
                } 
                
                ?>
            </div>
            </nav>
        </div>
        
        <div class="container">
        <br><br>

