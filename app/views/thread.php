<div class="container">
	<div class="panel">
		<div class="panel-body">
		 <?php 
            $ThreadById=$data['threadById'];
                foreach ($ThreadById as $value) {
                Session::set('threadId',$value['tid']);
             ?>
            <div class="jumbotron">
                <h2 class="post-heading"></h2>
                <div class="post-preview">
                    <p class="post-meta">

                        Published by <a href="<?php echo BASE_DIR?>/UserProfile/showUserProfileDetails/<?php echo $value['id'];?>"><?php echo $value['name']; ?></a>
                        On
                        <?php echo $value['created_at']; ?>
                    </p>
                    <h2 class="post-title">Title: <?php echo $value['title']; ?></h2>
                    
                </div>
                <hr>
                <?php echo $value['description']; ?>
            </div>

            
            <!--Thread Ends-->
            <?php } ?>  
		</div>
		<div class="panel-body">
			<form method="POST" class="interact well well-lg form-horizontal" action="<?php echo BASE_URL?>/Thread/showThreadAnswer/<?php echo Session::get('threadId');?>">
			<div class="form-group">
				<textarea name="answer" id="answer" class="form-control" cols="30" rows="6" placeholder="Type your Answer"></textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="btnAnswer" id="btnAnswer" value="Answer" class="btn btn-default">
			</div>
			</form>
		</div>

		<div class="panel-body">
			    <?php 
            $ThreadAnswer=$data['threadAnswer'];
                foreach ($ThreadAnswer as $value) {
             ?>
             <div class="panel panel-default interact well well-lg">
             	<div class="panel-body">
             		<a href="<?php echo BASE_DIR?>/UserProfile/showUserProfileDetails/<?php echo $value['id'];?>"><?php echo $value['name']?></a> answered
             		<br>
             		<br>
             		<?php echo $value['answer']; ?>
             	</div>
             	 <hr>
             	<div class="panel-body">
             	<form method="POST" class="interact well well-lg" action="<?php echo BASE_URL?>/Thread/showThreadReply/<?php echo Session::get('threadId');?>">
	             	<div class="form-group">
	             	<textarea id="reply" name="reply" cols="60" rows="2" placeholder="Type your Reply"></textarea>
	             	</div>
	             	<div class="form-group">
	             	<button id="btnReply" name="btnReply" class="btn btn-default" value="<?php echo $value['aid']; ?>">Reply</button>
	             	</div>
	             </form>
	             <?php $AnswerReply=$data[$value['aid']];
	             foreach ($AnswerReply as $value2) {
	             	//var_dump($value2);
	            	?>
	             		<li><a href="<?php echo BASE_DIR?>/UserProfile/showUserProfileDetails/<?php echo $value['id'];?>"><?php echo $value2['name']; ?>  </a>Replied<br>Reply:  <?php echo $value2['reply']; ?></li>
	             		<?php
	             	}
	            
	              ?>
             	</div>
             </div>
            <!--Thread Ends-->
            <?php } ?>  
		</div>
	</div>
</div>