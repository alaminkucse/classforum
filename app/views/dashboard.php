<br>

<div class="content">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>User Request</h1>
                    <hr class="small">
                </div>
            </div>
        </div>

<!-- Page content -->
<div id="page-content-wrapper" >
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset">
        <div class="row">

            <div class="container">

             <table class="table table-striped" align="center">
                 <tr>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Approve Login</th>
                     <th>Reject</th>
                     <th>Make Admin</th>
                 </tr>
                
                        <tbody>
                        <?php
                            foreach ($users as $key => $value) {

                        ?>
                        <tr>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo $value['email']; ?></td>
                            
                            <form method="post" action="<?php echo BASE_URL?>/Dashboard/adminController/<?php echo $value['id'];?>">
                                <?php if($value['is_approved'] == 0 && $value['is_admin'] == 0){ ?>
                                <td><button type="submit" name="approve" value="<?php echo $value['id']; ?>" class="btn btn-success">Approve</button></td>
                                <td><button type="submit" name="reject" value="<?php echo $value['id']; ?>" class="btn btn-danger">Reject</button></td>
                                <td><button type="submit" name="admin" value="<?php echo $value['id']; ?>" class="btn btn-primary">Admin</button></td>
                                <?php }else if($value['is_admin'] != 0) { ?>
                                <td><button class="btn btn-disable">Approved</button></td>
                                <td><button class="btn btn-disable">Delete</button></td>
                                <td><button type="submit" name="removeAdmin" value="<?php echo $value['id']; ?>" class="btn btn-primary">Remove Admin</button></td>
                                <?php }else  { ?>
                                <td><button class="btn btn-disable">Approved</button></td>
                                <td><button type="submit" name="reject" value="<?php echo $value['id']; ?>" class="btn btn-danger">Delete</button></td>
                                <td><button type="submit" name="admin" value="<?php echo $value['id']; ?>" class="btn btn-primary">Admin</button></td>
                                

                                <?php }?>

                                

                            </form>



                        </tr>
                        <?php } ?>
                        </tbody>

                
             </table>
            </div>

        </div>

    </div>
</div>
</div>

