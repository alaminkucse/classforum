

<br>
<br>

<div class="content">
    <div class="row" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-preview">
            
              <?php if($searchdata){
                foreach ($searchdata as $key => $value) {

                  ?>
                  
                  <a href="<?php echo BASE_DIR?>/Thread/showThreadDetails/<?php echo $value['tid'];?>" >
                  <h2 class="post-title"><?php echo $value['title']; ?></h2>
                  <h3 class="post-subtitle"><?php echo $value['description']; ?></h3></a>
                  
                  <p class="post-meta">

                      Posted By <a href="<?php echo BASE_DIR?>/UserProfile/showUserProfileDetails/<?php echo $value['id'];?>"><?php echo $value['name']; ?></a>  On <?php echo $value['created_at']; ?>  Topic Name: <?php echo $value['topicName']; ?> 
                    
                  </p>
                    <hr>
              
              <?php } } else {?>
              <br>
              <br>
              <div align="center">

                    <b><span class="text-danger" style="size: 300px">No result found!</span></b>

              </div>

              <?php } ?>
            
          </div>
        </div>
        
    </div>
</div>

   