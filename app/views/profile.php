

<div class="container">

</div>
    <br>
<div class="content" xmlns:v-on="http://www.w3.org/1999/xhtml">
<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-xs-4">

                <img class="img img-responsive img-thumbnail" src="<?php echo IMG_DIR ?>/uploads/d.jpg"
                     style="margin-top: 30px;">
            </div>
            <div class="col-xs-8">
                <!--User Detail Starts-->
                <div class="post-preview">
                    
                        <h2 class="post-title"><?php echo Session::get('name');?></h2>
                    
                </div>
                <h4>Email: <?php echo Session::get('email');?></h4>
                <h4><?php echo Session::get('count'); ?> Threads</h4>
                <h4>Bio-Data: <?php echo Session::get('bio'); ?></h4>
                <form method="POST" action="<?php echo BASE_URL?>/Profile/editProfile/<?php echo Session::get('id'); ?>">
                  
                  <input type="submit" name="edit" value="Edit Profile">
                </form>
                


            </div>
        </div>
        <hr>


    </div>


    <div class="container">
        <div class="row" xmlns:v-on="http://www.w3.org/1999/xhtml">
            <div class="col-lg-8 col-md-10 mx-auto">
              <div class="post-preview">
                
                  <?php
                    $UserThread=$data['userThread'];
                    foreach ($UserThread as $value) {

                      ?>
                      
                      <a href="<?php echo BASE_DIR?>/Thread/showThreadDetails/<?php echo $value['tid'];?>" >
                      <h2 class="post-title"><?php echo $value['title']; ?></h2>
                      <h3 class="post-subtitle"><?php echo $value['description']; ?></h3></a>
                      <br>
                      <a href="<?php echo BASE_DIR?>/Profile/deleteThread/<?php echo $value['tid'];?>" class="btn btn-danger btn-sm">
                          <span class="glyphicon glyphicon-remove"></span>            
                      </a>
                      <?php if($value['solve_status']=='Unsolved'){ ?>
                          <a href="<?php echo BASE_DIR?>/Profile/markSolved/<?php echo $value['tid'];?>" class="btn btn-success btn-sm">
                              <span>Solve</span>            
                          </a>
                        <?php }else {?>
                              <a href="<?php echo BASE_DIR?>/Profile/markUnsolved/<?php echo $value['tid'];?>" class="btn btn-danger btn-sm">
                                  <span >Unsolve</span>            
                              </a>
                        <?php } ?>
                      
                      <br>
                      <br>
                      <p class="post-meta">

                          Date: <?php echo $value['created_at']; ?>  Topic Name: <?php echo $value['topicName']; ?> 
                        
                      </p>
                      <hr>
                        
                  <?php } ?>
                
              </div>
            </div>
            
        </div>
    </div>



    </div>
</div>