<div class="content">
    <br>

    <div id="carouselIndicators" class="" data-ride="carousel">
      
      <div >
        <div >
        	<marquee><h2>A Place to Resolve the unresolved</h2></marquee>
          
          <img class="d-block w-100" src="<?php echo IMG_DIR ?>/home-bg.jpg">


        </div>
        
        
      </div>
      
      
    </div>

 

<br>


<?php 

    if (session_status() == PHP_SESSION_NONE) {
        Session::init();
    }
    if(Session::get("login") == true) {

  ?>


     <div class="col-md-4 well" id="vm" style="background: none; border: none; box-shadow: none" v-cloak>
        <h1 class="post-heading" style="margin-top: 30px;">Topic Name</h1>
        <hr class="small">
        <?php 
        $topic = $data['topic'];
        foreach($topic as $value){?>
        <ul class="list-group">

            <li class="list-group-item">
                <a href="<?php echo BASE_DIR?>/Search/showThreadByTopicName/<?php echo $value['topicName'];?>"><?php echo $value['topicName']; ?></a>
                
            </li>

        </ul>
        <?php }?>
    </div>
    <div class="container">
        <div class="row" xmlns:v-on="http://www.w3.org/1999/xhtml">
            <div class="col-lg-8 col-md-10 mx-auto">
              <div class="post-preview">
                
                  <?php
                    $threads = $data['threads'];
                    foreach ($threads as $key => $value) {

                      ?>
                      
                      <a href="<?php echo BASE_DIR?>/Thread/showThreadDetails/<?php echo $value['tid'];?>" >
                      <h2 class="post-title"><?php echo $value['title']; ?></h2>
                      <h3 class="post-subtitle"><?php echo $value['description']; ?></h3></a>
                      <br>
                      <span class="text-primary">Solution Status: <?php echo $value['solve_status']; ?></span>
                      <?php if(Session::get("is_admin")) {?>
                        <a href="<?php echo BASE_DIR?>/Profile/deleteThread/<?php echo $value['tid'];?>" class="btn btn-danger btn-sm">
                          <span class="glyphicon glyphicon-remove"></span>            
                      </a>
                      <?php if($value['solve_status']=='Unsolved'){ ?>
                          <a href="<?php echo BASE_DIR?>/Profile/markSolved/<?php echo $value['tid'];?>" class="btn btn-success btn-sm">
                              <span>Solve</span>            
                          </a>
                        <?php }else {?>
                              <a href="<?php echo BASE_DIR?>/Profile/markUnsolved/<?php echo $value['tid'];?>" class="btn btn-danger btn-sm">
                                  <span >Unsolve</span>            
                              </a>
                        <?php } ?>
                      <?php } ?>
                      <br>
                      <br>
                      <p class="post-meta">

                          Posted By <a href="<?php echo BASE_DIR?>/UserProfile/showUserProfileDetails/<?php echo $value['id'];?>"><?php echo $value['name']; ?></a>  On <?php echo $value['created_at']; ?>  Topic Name: <?php echo $value['topicName']; ?> 
                        
                      </p>
                        <hr>
                  <?php } ?>
                
              </div>
            </div>




            
        </div>
    </div>

    <?php } ?>




    
