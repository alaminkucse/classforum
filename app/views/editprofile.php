<br>
<br>
<div class="jumbotron">

	<header>
	<br>
		<center><h3>Edit Profile</h3></center>
	<br>
	</header>

	<div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <span class="text-success"><?php
                    if(isset($success)){
                        echo $success;
                    }
                    ?></span>
            </div>
      </div>
      <br>
      <br>
	<form method="POST" action="<?php echo BASE_URL?>/Profile/updateProfile/<?php echo Session::get('id'); ?>">
		
			<div class="form-group row">
			    <label for="name" class="col-sm-2 col-form-label">Name</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="name" value="<?php echo Session::get('name'); ?>" name="name" required>
			    </div>
			</div>
			
			<div class="form-group row">
			    <label for="bio" class="col-sm-2 col-form-label">Bio-Data</span></label>
			    <div class="col-sm-10">
			    	<textarea class="form-control" id="bio" name="bio" required><?php echo Session::get('bio'); ?></textarea>
			    </div>
			</div>
			
			
			<div class="form-group row">
			    <label for="email" class="col-sm-2 col-form-label">Email</span></label>
			    <div class="col-sm-10">
			    	<input type="email" class="form-control" id="email" value="<?php echo Session::get('email'); ?>" name="email" required>
			    </div>
			</div>

			
			<!-- <div class="form-group row">
			    <label for="inputPassword" class="col-sm-2 col-form-label">Confirm Password<span class="text-danger"> *</span></label>
			    <div class="col-sm-10">
			    	<input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" required>
			    </div>
			</div> -->
			<div>
			<center>
				<button type="submit" name="update" class="btn btn-outline-customs">Update</button>
			</center>
		</div>

	</form>
</div>