<div class="content">
    
        <header>
            <br>
                <center><h2>Create New Thread</h2></center>
            <br>
         </header>
       
        <form role="form" method="POST" class="interact well well-lg" action="<?php echo BASE_URL?>/ThreadCreate/newThreadCreate">
            
            
            <div class="form-group row">
                <label for="title" class="col-sm-2 col-form-label">Title<span class="text-danger"> *</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" placeholder="Thread Title" name="title" required>
                </div>
            </div>

            <div class="form-group row">
                
                <label for="topic" class="col-sm-2 col-form-label">Topic Name<span class="text-danger"> *</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="topic" placeholder="Thread Topic" name="topic" required>
                </div>
            
            </div>
            
            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label">Description<span class="text-danger"> *</span></label>
                <div class="col-sm-10">
                    <textarea class="form-control"
                          placeholder="Thread Description"
                          id="description" name="description" required
                          data-validation-required-message="Please enter description"></textarea>
                </div>
            </div>
            <div>
                <center>
                    <button type="submit" name="create" class="btn btn-outline-customs">Create</button>
                </center>
            </div>
        </form>

   
</div>