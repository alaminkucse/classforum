<?php

/**
 * Login Controller
 */
class Login extends MainController
{
    public function __construct(){
        parent::__construct();

    }

    public function Index(){
        $this->login();
    }

    public function login($error = false){
        Session::init();
        if(Session::get("login") == true ){
            header("Location: ".BASE_URL."/Index");
        } else {
            $data = ['pageName' => 'Log in'];
            $this->load->view("header", $data);
            array_push($data, $error);
            $this->load->view("auth/login", $data);
            $this->load->view("footer");
        }
    }

    public function loginAuth(){

        $error = [];
        if (isset($_POST['btn_login'])) {
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $table = "users";
            $loginModel = $this->load->model("LoginModel");
            $loginData  = $loginModel->getIdByUserEmailPass($email, $password, $table);
            if(!empty($loginData)) {
                Session::init();
                Session::set('login', 'true');
                Session::set('email', $loginData[0]['email']);
                Session::set('id',  $loginData[0]['id']);
                Session::set('name',  $loginData[0]['name']);
                Session::set('bio',  $loginData[0]['bio']);
                Session::set('is_approved',  $loginData[0]['is_approved']);
                Session::set('is_admin',  $loginData[0]['is_admin']);
                Session::set('password',  $loginData[0]['password']);
                header("Location: ".BASE_URL."/Index");
            }else{

                
                $data = ['pageName' => 'Log in'];
                $this->load->view("header", $data);
                $success_msg = ['fail' => 'Invalid Email or Password'];
                $this->load->view("auth/login", $success_msg);
                $this->load->view("footer");

                //header("Location: ".BASE_URL."/Login/login/".$success_msg);
                //self::login($error);
            }
        }
    }

    public function logOut(){
        Session::init();
        Session::destroy();
        header("Location: ".BASE_URL."/Login");
    }
}
