<?php

/**
* Thread Controller
*/
class UserProfile extends MainController
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Index(){
        //self::thread();
    }

   
    public function showUserProfileDetails($id)
    {
        $data = ['pageName' => 'UserProfile'];
        $this->load->view("header", $data);

        $data = array();
        $userProfileModel = $this->load->model("UserProfileModel");
        $data['userDetails'] = $userProfileModel->getUserById($id);
        $data['userThread'] = $userProfileModel->getAllthreadById($id);
        $this->load->view("userprofile",$data);
        $this->load->view("footer");
    }



    
    
    
    

}