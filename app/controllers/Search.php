<?php

/**
 * Search Controller
 */
class Search extends MainController
{

    public function __construct(){
        parent::__construct();
//        Session::checkSession();
    }

    public function Index(){
        $this->searching();
    }

    public function searching(){
        
        $data = ['pageName' => 'Search'];
        $this->load->view("header", $data);

        $searchModel = $this->load->model("SearchModel");
        $data['searchdata'] = $searchModel->searchInfo($_GET['search']);
        
        $this->load->view("search",$data);
        $this->load->view("footer");
    }

    public function showThreadByTopicName($topic)
    {
        
        $data = ['pageName' => 'Search'];
        $this->load->view("header", $data);

        $searchModel = $this->load->model("SearchModel");
        $data['searchdata'] = $searchModel->showThreadByTopic($topic);
        
        $this->load->view("search",$data);
        $this->load->view("footer");
    }




}