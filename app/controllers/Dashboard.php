<?php

/**
* Dashboard Controller
*/
class Dashboard extends MainController
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function Index()
	{
		//self::dashboard();
	}

	public function dashboard(){

        
        $data = ['pageName' => 'Admin Dashboard'];
        $this->load->view("header", $data);
        $dashModel = $this->load->model("DashboardModel");
        $data['users'] = $dashModel->getAllUsers();
        $this->load->view("dashboard",$data);
        $this->load->view("footer");
    }

    public function adminController($id)
    {

    	$dashModel = $this->load->model("DashboardModel");
        $cond = "id = $id";
    	
    	if(isset($_POST['approve']))
    	{
            $data = array(
                'is_approved' => '1'
                );

    		$approve = $dashModel->ApproveUser('users',$data,$cond);

    	}
    	else if(isset($_POST['reject']))
    	{
    		$reject = $dashModel->RejectUser('users', $cond);

    	}
    	else if(isset($_POST['admin']))
    	{

            $data = array(
                'is_approved' => '1',
                'is_admin' =>'1'
                );
    		$admin = $dashModel->MakeAdmin('users',$data,$cond);

    	}

        else if(isset($_POST['removeAdmin']))
        {

            $data = array(
                'is_admin' =>'0'
                );
            $removeAdmin = $dashModel->RemoveAdmin('users',$data,$cond);

        }

    	$data = ['pageName' => 'Admin Dashboard'];
        $this->load->view("header", $data);
        
        $data['users'] = $dashModel->getAllUsers();
        $this->load->view("dashboard",$data);
        $this->load->view("footer");


    }
}