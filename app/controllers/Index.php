<?php

/**
 * Index Controller
 */
class Index extends MainController
{

    public function __construct(){
        parent::__construct();
//        Session::checkSession();
    }

    public function Index(){
        $this->home();
    }

    public function home(){
        $data = ['pageName' => 'Home'];
        $this->load->view("header", $data);

        $threadModel = $this->load->model("ThreadModel");
        $data['threads'] = $threadModel->readThread();

        $data['topic'] = $threadModel->readThreadTopic();
        
        $this->load->view("home",$data);
        $this->load->view("footer");
    }

    

}