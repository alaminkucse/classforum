<?php


class Register extends MainController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Index(){
        self::register();
    }

    public function register($error = false){
        Session::init();
        if(Session::get("login") == true ){
            header("Location: ".BASE_URL."/Index");
        } else {
            $data = ['pageName' => 'Register'];
            $this->load->view("header", $data);
            $loadDropDown = $this->load->model("LoadDropDown");
            $sex = $loadDropDown->getSexes();
            

            array_push($data, $error);
            array_push($data, $sex);
            
            $this->load->view("auth/registration",$data);
            $this->load->view("footer");
        }
    }

    public function registerNewMember(){
        $name = $_POST['name'];
        $bio = $_POST['bio'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirmPassword'];
        $sex = $_POST['sex'];
        

        $error = [];

        
        if(empty($email)){
            array_push($error,"Email Required");
        }else{
            $userModel = $this->load->model('UserModel');
            $id = $userModel->getUserByEmail($email);
            if(!empty($id)){
                array_push($error, "Email already exist. please select another one");
            }
        }
        
        if($password != $confirmPassword){
            array_push($error, "Password doesn't matched");
        }
        if(!empty($error)){
            self::register($error);
        }
        else{
            $data = array(
                
                'name' => $name,
                'bio' => $bio,
                'email' => $email,
                'sex' => $sex,
                'password' => md5($password)
                
            );
            $msg = $userModel->insertIntoUser($data);
            
            if($msg != false){

                $data = ['pageName' => 'Log in'];
                $this->load->view("header", $data);
                $success_msg = ['success' => 'Your Request Submitted Successfully.'];
                $this->load->view("auth/login", $success_msg);
                $this->load->view("footer");

                //header("Location: ".BASE_URL."/Login/login/".$success_msg);


                

                //header("Location: ".BASE_URL."/Login/login/");
            } else{
                array_push($error, "Something went wrong. please try again later");
                self::register($error);
            }
            

        }
    }
}