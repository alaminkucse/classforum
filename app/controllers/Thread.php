<?php

/**
* Thread Controller
*/
class Thread extends MainController
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Index(){
        self::thread();
    }

   
    public function showThreadDetails($id)
    {
       $data = ['pageName' => 'Thread'];
        $this->load->view("header", $data);

        $data = array();
        $dataReply=array();
        $threadModel = $this->load->model("ThreadModel");
        $data['threadById'] = $threadModel->getThreadById($id);
        $data['threadAnswer'] = $threadModel->getAllanswer($id);
        foreach ($data['threadAnswer'] as $value) {

            $dataReply['reply']=$threadModel->getAnswerReply($value['aid']);    
            $data[$value['aid']]=$dataReply['reply'];
        }
        
        $this->load->view("thread",$data);
        $this->load->view("footer");
    }



    public function showThreadAnswer($id)
    {
        $answer = $_POST['answer'];
        Session::init();
        $user_id = Session::get('id');
        $thread_id = $id;
   
        $data = array(
        
        'answer' => $answer,
        'user_id' => $user_id,
        'thread_id' => $thread_id,
        
        );
        $threadModel = $this->load->model("ThreadModel");
        
        $msg = $threadModel->createThreadAnswer($data);
        
        
        if($msg != false){

            header("Location: ".BASE_URL."/Thread/showThreadDetails/$id");
        }
    }
    
    public function showThreadReply($id)
    {
        $answer_id=$_POST['btnReply'];
        $reply = $_POST['reply'];
        Session::init();
        $user_id = Session::get('id');
        $data = array(
        
        'reply' => $reply,
        'user_id' => $user_id,
        'answer_id' => $answer_id,
        
        );
        $threadModel = $this->load->model("ThreadModel");
        
        $msg = $threadModel->createAnswerReply($data);
        
        
        if($msg != false){

            header("Location: ".BASE_URL."/Thread/showThreadDetails/$id");
        }
    }


    
    

}