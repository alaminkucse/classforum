<?php

/**
 * Profile Controller
 */

class Profile extends MainController
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Index(){
        $this->profile();
    }

    public function profile(){
        Session::init();

        $data = ['pageName' => 'Profile'];
        $this->load->view("header", $data);
        $profileModel = $this->load->model("ProfileModel");
        $count = $profileModel->threadCount();
        Session::set('count', $count[0]['c']);

        $data['userThread'] = $profileModel->getAllthread();
        
        $this->load->view("profile",$data);
        $this->load->view("footer");
    }

    public function editProfile($id)
    {
        $data = ['pageName' => 'Edit Profile'];
        $this->load->view("header", $data);
        $this->load->view("editprofile");
        $this->load->view("footer");


    }
    public function updateProfile($id)
    {
        
        $cond = "id = $id";
        
        $name = $_POST['name'];
        $bio = $_POST['bio'];
        $email = $_POST['email'];
        //$password = md5($_POST['password']);
        $data = array(
        'name' => $name,
        'bio' => $bio,
        'email' =>$email
        );

        
        $profileModel = $this->load->model("ProfileModel");
        $updateinformation = $profileModel->updateInfo('users',$data,$cond);
        if($updateinformation !=false)
        {
            
            $loginData  = $profileModel->getUser($id);
            Session::init();
            
            Session::set('email', $loginData[0]['email']);
            Session::set('id',  $loginData[0]['id']);
            Session::set('name',  $loginData[0]['name']);
            Session::set('bio',  $loginData[0]['bio']);
            Session::set('is_approved',  $loginData[0]['is_approved']);
            Session::set('is_admin',  $loginData[0]['is_admin']);
            Session::set('password',  $loginData[0]['password']);
        }

        $data = ['pageName' => 'Profile'];
        $this->load->view("header", $data);
        $success_msg = ['success' => 'Your Profile Update Successfully.'];
        $this->load->view("editprofile",$success_msg);
        $this->load->view("footer");

    }

    public function deleteThread($tid)
    {
        $cond = "tid = $tid";
        $profileModel = $this->load->model("ProfileModel");
        
        $msg = $profileModel->deleteThreadbyId('threads', $cond);

        Session::init();

        $data = ['pageName' => 'Profile'];
        $this->load->view("header", $data);
        
        $count = $profileModel->threadCount();
        Session::set('count', $count[0]['c']);

        $data['userThread'] = $profileModel->getAllthread();
        
        $this->load->view("profile",$data);
        $this->load->view("footer");
        
    }

    public function markSolved($tid)
    {
        $cond = "tid = $tid";
        $profileModel = $this->load->model("ProfileModel");
        
        $data = array(
            'solve_status' => 'Solved'
        );

        $solved = $profileModel->Solved('threads',$data,$cond);

        Session::init();

        $data = ['pageName' => 'Profile'];
        $this->load->view("header", $data);
        
        $count = $profileModel->threadCount();
        Session::set('count', $count[0]['c']);

        $data['userThread'] = $profileModel->getAllthread();
        
        $this->load->view("profile",$data);
        $this->load->view("footer");
        
    }


    public function markUnsolved($tid)
    {
        $cond = "tid = $tid";
        $profileModel = $this->load->model("ProfileModel");
        
        $data = array(
            'solve_status' => 'Unsolved'
        );

        $unsolved = $profileModel->UnSolved('threads',$data,$cond);

        Session::init();

        $data = ['pageName' => 'Profile'];
        $this->load->view("header", $data);
        
        $count = $profileModel->threadCount();
        Session::set('count', $count[0]['c']);

        $data['userThread'] = $profileModel->getAllthread();
        
        $this->load->view("profile",$data);
        $this->load->view("footer");
        
    }


}