<?php

/**
 * ThreadCreate Controller
 */

class ThreadCreate extends MainController
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Index(){
        self::threadCreate();
    }

    public function threadCreate(){

        
        $data = ['pageName' => 'Thread Create'];
        $this->load->view("header", $data);
        $this->load->view("threadcreate");
        $this->load->view("footer");
    }


    public function newThreadCreate(){

        Session::init();
        $title = $_POST['title'];
        $topic = $_POST['topic'];
        $description = $_POST['description'];
        $created_at = date("Y/m/d");
        $user_id = Session::get('id');
   
        $data = array(
        
        'title' => $title,
        'description' => $description,
        'user_id' => $user_id,
        'topicName' => $topic,
        'created_at' => $created_at 
        );
        
        $threadModel = $this->load->model('ThreadModel');
        $msg = $threadModel->createThread($data);
        
        if($msg != false){

            header("Location: ".BASE_URL."/Index");
        }

    }
    
}