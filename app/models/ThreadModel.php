<?php

/**
* Thread Model
*/
class ThreadModel extends DBCon
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function createThread($data)
    {
        return $this->db->insert('threads', $data, true);
    }

    public function readThread()
    {
    	$sql = "SELECT * FROM threads,users WHERE users.id = threads.user_id order by threads.created_at desc";
        
        return $this->db->select($sql);
    }

    public function getThreadById($id)
    {
        $sql = "SELECT * FROM threads,users WHERE users.id = threads.user_id AND threads.tid = $id";
        
        return $this->db->select($sql);
    }

    public function getAllanswer($id)
    {

         //$sql = "SELECT * FROM answers WHERE answers.thread_id = $id ORDER BY answers.aid DESC";
        
        $sql = "SELECT * FROM answers INNER JOIN users ON users.id = answers.user_id AND answers.thread_id = $id ORDER BY answers.aid DESC";
        
        return $this->db->select($sql);
    }

    public function getAllreply($aid)
    {
        $sql = "SELECT * FROM replies WHERE replies.answer_id = $aid";
        
        return $this->db->select($sql);
    }
    public function createThreadAnswer($data)
    {
        return $this->db->insert('answers', $data, true);
    }
    public function createAnswerReply($data)
    {
        return $this->db->insert('replies', $data, true);
    }
    public function getAnswerReply($aid){
        //$sql="SELECT * FROM replies WHERE replies.answer_id=$aid";
        $sql = "SELECT * FROM replies INNER JOIN users ON users.id = replies.user_id AND replies.answer_id=$aid";

        return $this->db->select($sql);
    }

    public function readThreadTopic(){
        
        $sql = "SELECT distinct(topicName) FROM threads order by topicName";

        return $this->db->select($sql);
    }


   
    
}