<?php

/**
 * Profile Model
 */
class ProfileModel extends DBCon
{
    public function __construct(){
        parent::__construct();
    }

    public function threadCount(){
        //Session::init();
        $uid = Session::get('id');
        $sql = "SELECT count(tid) as c FROM threads WHERE user_id = :uid";
        $data = array(
            ':uid' => $uid
        );
        return $this->db->select($sql,$data);
    }

    public function getAllthread(){
        //Session::init();
        $uid = Session::get('id');
        $sql = "SELECT * FROM threads WHERE user_id = $uid";
        
        return $this->db->select($sql);
    }
    public function getUser($id)
    {
        $sql = "SELECT * FROM users WHERE id = $id";
        return $this->db->select($sql);
    }

    public function updateInfo($table, $data, $cond){

        return $this->db->update($table, $data, $cond);
    }

    public function deleteThreadbyId($table, $cond )
    {
        return $this->db->delete($table, $cond);
    }

    public function Solved($table, $data, $cond)
    {
        return $this->db->update($table, $data, $cond);
    }
    public function UnSolved($table, $data, $cond)
    {
        return $this->db->update($table, $data, $cond);
    }


}