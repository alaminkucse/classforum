<?php

/**
 * Search Model
 */
class SearchModel extends DBCon
{
    public function __construct(){
        parent::__construct();
    }

    public function searchInfo($searchtext){
        
        $sql = "SELECT * FROM threads as t INNER JOIN users as u WHERE u.id = t.user_id AND (t.title Like '%$searchtext%' OR t.description Like '%$searchtext%' OR t.topicName Like '%$searchtext%' OR u.name Like '%$searchtext%')";
        
        return $this->db->select($sql);
    }

    public function showThreadByTopic($topic){
        
        $sql = "SELECT * FROM threads as t INNER JOIN users as u WHERE u.id = t.user_id and t.topicName='$topic'";
        
        return $this->db->select($sql);
    }


}