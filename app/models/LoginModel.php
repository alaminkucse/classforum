<?php

/**
 * LoginModel Model
 */
class LoginModel extends DBCon
{
    public function __construct(){
        parent::__construct();
    }

    public function getIdByUserEmailPass($email, $pass, $table){
        $sql = "SELECT * FROM $table WHERE email = :email AND password = :password AND is_approved ='1'";
        $data = array(
            ":email" => $email,
            ":password" => $pass
        );
        return $this->db->select($sql, $data);
    }

}