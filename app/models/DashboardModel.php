<?php

/**
*  DashboardModel
*/
class DashboardModel extends DBCon
{
    
    public function __construct(){
        parent::__construct();
    }

    

    public function getAllUsers()
    {
        $sql = "SELECT * FROM users";
        
        return $this->db->select($sql);
    }

    public function ApproveUser($table, $data, $cond)
    {
        //$sql = "UPDATE users set is_approved = '1' where id = $id";

        return $this->db->update($table, $data, $cond);
    }

    public function RejectUser($table, $cond)
    {
        return $this->db->delete($table, $cond);
    }
    public function MakeAdmin($table, $data, $cond)
    {
        return $this->db->update($table, $data, $cond);
    }
    public function RemoveAdmin($table, $data, $cond)
    {
        return $this->db->update($table, $data, $cond);
    }
}