<?php


class UserModel extends DBCon{
    public function __construct(){
        parent::__construct();
    }

    public function getUserByEmail($email){
        $sql = "SELECT * FROM users WHERE email = :email";
        $data = array(
            ':email' => $email
        );
        return $this->db->select($sql, $data);
    }
    

    public function insertIntoUser($data){
        return $this->db->insert('users', $data, true);
    }

    public function getAllUsers()
    {
        $sql = "SELECT * FROM users";
        
        return $this->db->select($sql);
    }


   
    public function UpdatePassword($data){
        $id = self::getUserId();
        $cond = "id = $id";
        $table = 'user';
        if($this->db->update($table, $data, $cond)){
            Session::set("password", $data['password']);
            return true;
        }else {
            return false;
        }
    }

    public function getEmail()
    {
        return Session::get('email');
    }


    public function getUserId()
    {
        return Session::get('id');
    }

    public function getPassword()
    {
        return Session::get('password');
    }
}
